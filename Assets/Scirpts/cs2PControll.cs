﻿using UnityEngine;
using System.Collections;

public class cs2PControll : MonoBehaviour {
	// SetInteger // 0:오른쪽대기, 1:왼쪽대기, 2:왼쪽달리기, 3:오른쪽달리기
	private Animator aCharacterManager;

			public float fSpeed;	// 이동 속도 - 수치조정 가능
			public int 	 nMyFocus;	// 0:우 // 1:좌
	static  public int 	 n2pPoint;	// 2P의 점수
	static	public int   n2PSkillGauge; // 2P의 스킬게이지, 3이 full
	static  public bool  b2PSkillCheck;	// 2P 스킬 사용여부

	Vector3 startPos;

	public GameObject pf2P_Character;

	// Use this for initialization
	void Start () 
	{
		fSpeed    	= 2.0f;
		nMyFocus	= 1;
		n2pPoint   = 0;
		n2PSkillGauge = 0;
		b2PSkillCheck = false;

		aCharacterManager = this.GetComponent <Animator> ();
		// 2P의 시작위치
		startPos = new Vector3 (2.5f, 0.35f, 2.5f);
	}

	// Update is called once per frame
	void Update ()
	{
		if (csTime.nTimecnt > 0)	{
			CharacterMove();
			if (this.transform.position.y <= -10) {
				DeadAndResurrection ();
			}
			if (Input.GetKey(KeyCode.RightShift) && n2PSkillGauge == 3) {
				b2PSkillCheck = true;
			}
		}
	}

	void CharacterMove()
	{
		float fAmtToMove = fSpeed * Time.deltaTime;
		float fVerKey = Input.GetAxis("2p_Vertical");		// 수직 키입력
		float fHorKey = Input.GetAxis("2p_Horizontal");		// 수평 키입력

		if (this.transform.position.y > 0.3) {
			aCharacterManager.SetInteger ("FallLorR", 3);	// 초기화
			if (nMyFocus == 1) {		// 좌측대기
				aCharacterManager.SetInteger ("Direction", 1);
			} else {					// 우측대기
				aCharacterManager.SetInteger ("Direction", 0);
			} 

			if (fHorKey > 0) {							// 오른쪽 달리기
				aCharacterManager.SetInteger ("Direction", 3);
				nMyFocus = 0;
			} else if (fHorKey < 0) {					// 왼쪽 달리기
				aCharacterManager.SetInteger ("Direction", 2);
				nMyFocus = 1;
			} else if (fVerKey > 0) {	// 상향키입력
				if (nMyFocus == 0) {	// 우측보고 위로달리기
				aCharacterManager.SetInteger ("Direction", 3);
				} else {				// 좌측보고 위로달리기
				aCharacterManager.SetInteger ("Direction", 2);
				}
			} else if (fVerKey < 0) {	//하향키입력
				if (nMyFocus == 0) {	//우측보고 밑에달리기
					aCharacterManager.SetInteger ("Direction", 3);
				} else {				// 좌측보고 밑에달리기
					aCharacterManager.SetInteger ("Direction", 2);
				}
			} 
		} else if (this.transform.position.y <= 0.3) {
			if (nMyFocus == 0) {
				aCharacterManager.SetInteger ("FallLorR", 0);
			} else {
				aCharacterManager.SetInteger ("FallLorR", 1);	
			}
		}
		transform.Translate (Vector3.forward * fAmtToMove * fVerKey);
		transform.Translate (Vector3.right * fAmtToMove * fHorKey);
	}
	void DeadAndResurrection()
	{
		this.transform.position = startPos;
	}
}
