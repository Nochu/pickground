﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class cs1PSkillGauge : MonoBehaviour {

	public Image  p1NowGauge;

	// Use this for initialization
	void Start () {
		p1NowGauge.sprite = Resources.Load<Sprite> ("gauge_left_off");
	}

	// Update is called once per frame
	void Update () {
		if (cs1PControll.n1PSkillGauge == 1 && this.tag == "1P_G_1") {
			p1NowGauge.sprite = Resources.Load<Sprite> ("gauge_left_on");
		} else if (cs1PControll.n1PSkillGauge == 2 && this.tag == "1P_G_2") {
			p1NowGauge.sprite = Resources.Load<Sprite> ("gauge_left_on");
		} else if (cs1PControll.n1PSkillGauge == 3 && this.tag == "1P_G_3") {
			p1NowGauge.sprite = Resources.Load<Sprite> ("gauge_left_on");
		}
		if (cs1PControll.n1PSkillGauge == 0) {
			p1NowGauge.sprite = Resources.Load<Sprite> ("gauge_left_off");
		}
	}
}