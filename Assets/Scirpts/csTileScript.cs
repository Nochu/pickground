﻿using UnityEngine;
using System.Collections;

public class csTileScript : MonoBehaviour {
	//NomalTile, 1PTile, 2PTile의 타일이미지
	public Animator	aTileManager;

	//Nomal:0 / 1P:1 / 2P:2
	public int nTileCheck;

	// Use this for initialization
	void Start () {
		aTileManager = gameObject.GetComponent<Animator>();
		aTileManager.SetInteger ("whosTile", 0);
		if (gameObject.tag == "TILE")
			nTileCheck = 0;	//1: 1P // 2: 2P // 3: only1P(non2P) // 4: only2P(non1P)
	}

	// Update is called once per frame

	void OnTriggerEnter(Collider _cother)				// Sphere Collider
	{
		if ((nTileCheck == 0 || nTileCheck == 2) && 	// 1P의 땅획득
			_cother.gameObject.tag == "PLAYER_1") 
		{
			if (nTileCheck == 2) {	//2P의 땅을먹으면 2P의 점수 차감
				cs2PControll.n2pPoint--;
			} 
			cs1PControll.n1pPoint++;
			nTileCheck = 1;
			cs1PControll.n1PSkillGauge++;
			if (cs1PControll.n1PSkillGauge > 3) {
				cs1PControll.n1PSkillGauge = 3;
			}
			aTileManager.SetInteger ("whosTile", 1);
		}

		if ((nTileCheck == 0 || nTileCheck == 1) &&
			_cother.gameObject.tag == "PLAYER_2") 		// 2P의 땅획득
		{
			if (nTileCheck == 1) {
				cs1PControll.n1pPoint--;
			}
			cs2PControll.n2pPoint++;
			nTileCheck = 2;
			cs2PControll.n2PSkillGauge++;
			if (cs2PControll.n2PSkillGauge > 3) {
				cs2PControll.n2PSkillGauge = 3;
			}
			aTileManager.SetInteger ("whosTile", 2);
		}
		if ((nTileCheck == 0 || nTileCheck == 1 || nTileCheck == 2) &&		// 1P의 완전점령
		    _cother.gameObject.tag == "PLAYER_1" &&
		    cs1PControll.b1PSkillCheck == true) {
			nTileCheck = 3;
			cs1PControll.n1PSkillGauge = 0;
			cs1PControll.b1PSkillCheck = false;
		}
		if ((nTileCheck == 0 || nTileCheck == 1 || nTileCheck == 2) &&		// 2P의 완전점령
			_cother.gameObject.tag == "PLAYER_2" &&
			cs2PControll.b2PSkillCheck == true) {
			nTileCheck = 4;
			cs2PControll.n2PSkillGauge = 0;
			cs2PControll.b2PSkillCheck = false;
		}
	}
}