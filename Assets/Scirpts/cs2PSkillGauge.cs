﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class cs2PSkillGauge : MonoBehaviour {

	public Image  p2NowGauge;

	// Use this for initialization
	void Start () {
		p2NowGauge.sprite = Resources.Load<Sprite> ("gauge_right_off");
	}

	// Update is called once per frame
	void Update () {
		if (cs2PControll.n2PSkillGauge == 1 && this.tag == "2P_G_1") {
			p2NowGauge.sprite = Resources.Load<Sprite> ("gauge_right_on");
		} else if (cs2PControll.n2PSkillGauge == 2 && this.tag == "2P_G_2") {
			p2NowGauge.sprite = Resources.Load<Sprite> ("gauge_right_on");
		} else if (cs2PControll.n2PSkillGauge == 3 && this.tag == "2P_G_3") {
			p2NowGauge.sprite = Resources.Load<Sprite> ("gauge_right_on");
		}
		if (cs2PControll.n2PSkillGauge == 0) {
			p2NowGauge.sprite = Resources.Load<Sprite> ("gauge_right_off");
		}
	}
}