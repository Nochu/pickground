﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class csTime : MonoBehaviour {

	public Text timeText;
	static public float  nTimecnt;

	// Use this for initialization
	void Start () {
		nTimecnt = 60;

		timeText = GetComponent<Text> ();
	}

	// Update is called once per frame
	void Update () {
		nTimecnt -= Time.deltaTime;
		if (nTimecnt < 0)
			nTimecnt = 0;
		timeText.text = nTimecnt.ToString ("00");
	}
}