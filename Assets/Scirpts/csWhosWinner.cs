﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class csWhosWinner : MonoBehaviour {

	public Text GameOver;

	// Use this for initialization
	void Start () {
		GameOver.text = "";
	}
	
	// Update is called once per frame
	void Update () {
		if (csTime.nTimecnt <= 0) {
			if (cs1PControll.n1pPoint > cs2PControll.n2pPoint)
				GameOver.text = "<color=#FD8AFFFF>" + "Player 1 is Winner!!" + "</color>";
			else if (cs1PControll.n1pPoint < cs2PControll.n2pPoint)
				GameOver.text = "<color=#00F4FFFF>" + "Player 2 is Winner!!" + "</color>";
 			else if (cs1PControll.n1pPoint == cs2PControll.n2pPoint)
				GameOver.text = "<color=#FF6400FF>" + "D R A W" + "</color>";

			Invoke ("Restart", 3);
		}
	}

	void Restart()
	{
		Application.LoadLevel ("MainGame");
	}
}
